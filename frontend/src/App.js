import React, { Fragment } from 'react';
import Login from './paginas/auth/Login';
import Home from './paginas/Home';

//componente Fradgmente como contenedor
/**Configuración básica de React Router
Una vez instalado, podemos traer nuestro primer componente 
que se requiere para usar React router, que se llama BrowserRouter.
Ten en cuenta que hay varios tipos de enrutadores que proporciona react-enrutador-dom, además de BrowserRouter, en el que no entraremos. Es una práctica común poner un alias (renombrar) BrowserRoute simplemente como 'Router' cuando se importa. */
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import CrearCuenta from './paginas/auth/CrearCuenta';
import ProductosAdmin from './paginas/productos/ProductosAdmin';
import ProductosCrear from './paginas/productos/ProductosCrear';
import ProductosEditar from './paginas/productos/ProductosEditar'
import CategoriasAdmin from './paginas/categoria/CategoriasAdmin';
import CategoriasCrear from './paginas/categoria/CategoriasCrear';
import CategoriasEditar from './paginas/categoria/CategoriasEditar';
import ProveedoresAdmin from './paginas/proveedores/ProveedoresAdmin';
import ProveedoresCrear from './paginas/proveedores/ProveedoresCrear';
import ProveedoresEditar from './paginas/proveedores/ProveedoresEditar';
import ComprasAdmin from './paginas/compras/ComprasAdmin';
import ComprasCrear from './paginas/compras/ComprasCrear';
import ComprasEditar from './paginas/compras/ComprasEditar';
import FacturasAdmin from './paginas/facturas/FacturasAdmin';
import FacturasCrear from './paginas/facturas/FacturasCrear';
import FacturasEditar from './paginas/facturas/FacturasEditar';
import EmpleadosAdmin from './paginas/empleados/EmpleadosAdmin';
import EmpleadosCrear from './paginas/empleados/EmpleadosCrear';
import EmpleadosEditar from './paginas/empleados/EmpleadosEditar';


/**  Declaramos rutas dentro del componente Router como secundarias. Podemos declarar tantas rutas como queramos y necesitamos proporcionar al menos dos propiedades para cada ruta, path y component (o render): */
function App() {
  return (
    <Fragment>
      <Router>
        <Routes>
          <Route path="/" exact element={<Login />} />
          <Route path="/crear-cuenta" exact element={<CrearCuenta />} />
          <Route path="/home" exact element={<Home />} />
          <Route path="/productos-admin" exact element={<ProductosAdmin />} />
          <Route path="/productos-crear" exact element={<ProductosCrear />} />
          <Route path="/productos-editar/:idproducto" exact element={<ProductosEditar />} />
          <Route path='/categorias-admin' exact element={<CategoriasAdmin />} />
          <Route path='/categorias-crear' exact element={<CategoriasCrear />} />
          <Route path='/categorias-editar/:idcategoria' exact element={<CategoriasEditar />} />
          <Route path="/facturas-admin" exact element={<FacturasAdmin />} />
          <Route path="/facturas-crear" exact element={<FacturasCrear />} />
          <Route path="/facturas-editar/:idfactura" exact element={<FacturasEditar />} />
          <Route path="/empleados-admin" exact element={<EmpleadosAdmin />} />
          <Route path="/empleado-crear" exact element={<EmpleadosCrear />} />
          <Route path="/empleados-editar/:idempleado" exact element={<EmpleadosEditar />} />
          <Route path='/proveedores-admin' exact element={<ProveedoresAdmin />} />
          <Route path='/proveedores-crear' exact element={<ProveedoresCrear />} />
          <Route path='/proveedores-editar/:idproveedor' exact element={<ProveedoresEditar />} />
          <Route path='/compras-admin' exact element={<ComprasAdmin />} />
          <Route path='/compras-crear' exact element={<ComprasCrear />} />
          <Route path='/compras-editar/:idcompra' exact element={<ComprasEditar />} />

        </Routes>

      </Router>
    </Fragment>
  );
}

export default App;
