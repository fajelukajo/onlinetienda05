import React, { useState, useEffect } from 'react';
import ContentHeader from '../../componentes/ContentHeader';
import Footer from '../../componentes/Footer';
import Navbar from '../../componentes/Navbar';
import SidebarContainer from '../../componentes/SidebarContainer';
import { useNavigate } from 'react-router-dom';
import APIInvoke from '../../utils/APIInvoke'
import swal from 'sweetalert';

const FacturasCrear = () => {

    const navigate = useNavigate();

    const [factura, setFactura] = useState({
        fecha: '',
        nombre_cliente: '',
        documento_cliente: 0,
        telefono_cliente: 0,
        direccion_cliente: '',
        codigo_producto: 0,
        cantidad: 0,
        descuento: 0,
        forma_de_pagos: '',
        valor_unitario: 0,
        subtotal: 0,
        IVA: 0,
        valor_total: 0
    });

    const { fecha, nombre_cliente, documento_cliente, telefono_cliente, direccion_cliente, codigo_producto, cantidad, descuento, forma_de_pagos, valor_unitario, subtotal, IVA, valor_total  } = factura;

    useEffect(() => {
        document.getElementById("fecha").focus();
    }, [])

    const onChange = (e) => {
        setFactura({
            ...factura,
            [e.target.name]: e.target.value
        })
    }

    const crearFactura = async () => {
        const data = {
            fecha: factura.fecha,
            nombre_cliente: factura.nombre_cliente,
            documento_cliente: factura.documento_cliente,
            telefono_cliente: factura.telefono_cliente,
            direccion_cliente: factura.direccion_cliente,
            codigo_producto: factura.codigo_producto,
            cantidad: factura.cantidad,
            descuento: factura.descuento,
            forma_de_pagos: factura.forma_de_pagos,
            valor_unitario: factura.valor_unitario,
            subtotal: factura.subtotal,
            IVA: factura.IVA,
            valor_total: factura.valor_total

        }

        console.log(JSON.stringify(data))
        const response = await APIInvoke.invokePOST(`/api/factura`, data);
        const idFactura = response._id;

        if (idFactura === '') {
            const msg = "La factura NO fue creada correctamente.";
            swal({
                title: 'Error',
                text: msg,
                icon: 'error',
                buttons: {
                    confirm: {
                        text: 'Ok',
                        value: true,
                        visible: true,
                        className: 'btn btn-danger',
                        closeModal: true
                    }
                }
            });
        } else {
            navigate("/facturas-admin");
            const msg = "La factura fue creada correctamente.";
            swal({
                title: 'Información',
                text: msg,
                icon: 'success',
                buttons: {
                    confirm: {
                        text: 'Ok',
                        value: true,
                        visible: true,
                        className: 'btn btn-primary',
                        closeModal: true
                    }
                }
            });
            setFactura({
                fecha: '',
                nombre_cliente: '',
                documento_cliente: 0,
                telefono_cliente: 0,
                direccion_cliente: '',
                codigo_producto: 0,
                cantidad: 0,
                descuento: 0,
                forma_de_pagos: '',
                valor_unitario: 0,
                subtotal: 0,
                IVA: 0,
                valor_total: ''
             })
        }
    }

    const onSubmit = (e) => {
        e.preventDefault();
        crearFactura();
    }

    return (
        <div className="wrapper">
            <Navbar></Navbar>
            <SidebarContainer></SidebarContainer>
            <div className="content-wrapper">

                <ContentHeader
                    titulo={"Creación de Facturas"}
                    breadCrumb1={"Listado de Facturas"}
                    breadCrumb2={"Creación"}
                    ruta1={"/facturas-admin"}
                />

                <section className="content">
                    <div className="card">
                        <div className="card-header">
                            <div className="card-tools">
                                <button type="button" className="btn btn-tool" data-card-widget="collapse" title="Collapse">
                                    <i className="fas fa-minus" />
                                </button>
                                <button type="button" className="btn btn-tool" data-card-widget="remove" title="Remove">
                                    <i className="fas fa-times" />
                                </button>
                            </div>
                        </div>
                        <div className="card-body">

                        <form onSubmit={onSubmit}>
                            <div className="card-body">
                                    <div className="form-group">
                                        <label htmlFor="nombre">Fecha</label>
                                        <input type="text"
                                            className="form-control"
                                            id="fecha"
                                            name="fecha"
                                            placeholder="Ingrese la fecha"
                                            value={fecha}
                                            onChange={onChange}
                                            required
                                        />
                                    </div>
                                </div>

                                <div className="card-body">
                                    <div className="form-group">
                                        <label htmlFor="nombre">Nombre Cliente</label>
                                        <input type="text"
                                            className="form-control"
                                            id="nombre_cliente"
                                            name="nombre_cliente"
                                            placeholder="Ingrese el nobre del cliente"
                                            value={nombre_cliente}
                                            onChange={onChange}
                                            required
                                        />
                                    </div>
                                </div>

                                <div className="card-body">
                                    <div className="form-group">
                                        <label htmlFor="nombre">Documento Cliente</label>
                                        <input type="number"
                                            className="form-control"
                                            id="documento_cliente"
                                            name="documento_cliente"
                                            placeholder="Ingrese el documento del cliente"
                                            value={documento_cliente}
                                            onChange={onChange}
                                            required
                                        />
                                    </div>
                                </div>

                                <div className="card-body">
                                    <div className="form-group">
                                        <label htmlFor="nombre">Telefono Cliente</label>
                                        <input type="number"
                                            className="form-control"
                                            id="telefono_cliente"
                                            name="telefono_cliente"
                                            placeholder="Ingrese el telefono_cliente"
                                            value={telefono_cliente}
                                            onChange={onChange}
                                            required
                                        />
                                    </div>
                                </div>

                                <div className="card-body">
                                    <div className="form-group">
                                        <label htmlFor="nombre">Direccion Cliente</label>
                                        <input type="text"
                                            className="form-control"
                                            id="direccion_cliente"
                                            name="direccion_cliente"
                                            placeholder="Ingrese la direccion_cliente"
                                            value={direccion_cliente}
                                            onChange={onChange}
                                            required
                                        />
                                    </div>
                                </div>

                                <div className="card-body">
                                    <div className="form-group">
                                        <label htmlFor="nombre">Codigo Producto</label>
                                        <input type="number"
                                            className="form-control"
                                            id="codigo_producto"
                                            name="codigo_producto"
                                            placeholder="Ingrese el codigo_producto"
                                            value={codigo_producto}
                                            onChange={onChange}
                                            required
                                        />
                                    </div>
                                </div>
                            
                                <div className="card-body">
                                    <div className="form-group">
                                        <label htmlFor="nombre">Cantidad</label>
                                        <input type="number"
                                            className="form-control"
                                            id="cantidad"
                                            name="cantidad"
                                            placeholder="Ingrese la cantidad"
                                            value={cantidad}
                                            onChange={onChange}
                                            required
                                        />
                                    </div>
                                </div>

                                <div className="card-body">
                                    <div className="form-group">
                                        <label htmlFor="nombre">Descuento</label>
                                        <input type="number"
                                            className="form-control"
                                            id="descuento"
                                            name="descuento"
                                            placeholder="Ingrese el descuento"
                                            value={descuento}
                                            onChange={onChange}
                                            required
                                        />
                                    </div>
                                </div>

                                <div className="card-body">
                                    <div className="form-group">
                                        <label htmlFor="nombre">Forma de pagos</label>
                                        <input type="text"
                                            className="form-control"
                                            id="forma_de_pagos"
                                            name="forma_de_pagos"
                                            placeholder="Ingrese la forma_de_pagos"
                                            value={forma_de_pagos}
                                            onChange={onChange}
                                            required
                                        />
                                    </div>
                                </div>

                                <div className="card-body">
                                    <div className="form-group">
                                        <label htmlFor="nombre">valor unitario</label>
                                        <input type="number"
                                            className="form-control"
                                            id="valor_unitario"
                                            name="valor_unitario"
                                            placeholder="Ingrese el valor unitario"
                                            value={valor_unitario}
                                            onChange={onChange}
                                            required
                                        />
                                    </div>
                                </div>

                                <div className="card-body">
                                    <div className="form-group">
                                        <label htmlFor="nombre">Subtotal</label>
                                        <input type="number"
                                            className="form-control"
                                            id="subtotal"
                                            name="subtotal"
                                            placeholder="Ingrese el subtotal"
                                            value={subtotal}
                                            onChange={onChange}
                                            required
                                        />
                                    </div>
                                </div>

                                <div className="card-body">
                                    <div className="form-group">
                                        <label htmlFor="nombre">IVA</label>
                                        <input type="number"
                                            className="form-control"
                                            id="iva"
                                            name="iva"
                                            placeholder="Ingrese el IVA"
                                            value={IVA}
                                            onChange={onChange}
                                            required
                                        />
                                    </div>
                                </div>

                                <div className="card-body">
                                    <div className="form-group">
                                        <label htmlFor="nombre">Valor Total</label>
                                        <input type="number"
                                            className="form-control"
                                            id="valor_total"
                                            name="valor_total"
                                            placeholder="valor total"
                                            value={valor_total}
                                            onChange={onChange}
                                            required
                                        />
                                    </div>
                                </div>
                                <div className="card-footer">
                                    <button type="submit" className="btn btn-primary">Crear</button>
                                </div>
                            </form>

                        </div>
                    </div>
                </section>
            </div>
            <Footer></Footer>
        </div>
    );
}

export default FacturasCrear;