import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import ContentHeader from "../../componentes/ContentHeader";
import Footer from "../../componentes/Footer";
import Navbar from "../../componentes/Navbar";
import SidebarContainer from "../../componentes/SidebarContainer";
import APIInvoke from "../../utils/APIInvoke";
import swal from "sweetalert";

const FacturasAdmin = () => {
    
    const [facturas, setFacturas] = useState([]);

    const cargarFacturas = async () => {
        const response = await APIInvoke.invokeGET("/api/factura");
        //console.log(response);
        setFacturas(response);
    };

    useEffect(() => {
        cargarFacturas();
    }, []);

    //Eliminar proyectos
    const eliminarFactura = async (e, idFactura) => {
        e.preventDefault();
        const response = await APIInvoke.invokeDELETE(`/api/factura/${idFactura}`);

        if (response.msg === 'factura eliminada con exito') {
            const msg = "La factura fue borrada correctamente.";
            swal({
                title: 'Información',
                text: msg,
                icon: 'success',
                buttons: {
                    confirm: {
                        text: 'Ok',
                        value: true,
                        visible: true,
                        className: 'btn btn-primary',
                        closeModal: true
                    }
                }
            });
            cargarFacturas();
        } else {
            const msg = "La factura no fue borrada correctamente.";
            swal({
                title: 'Error',
                text: msg,
                icon: 'error',
                buttons: {
                    confirm: {
                        text: 'Ok',
                        value: true,
                        visible: true,
                        className: 'btn btn-danger',
                        closeModal: true
                    }
                }
            });
        }

    }

    return (
        <div className="wrapper">
            <Navbar></Navbar>
            <SidebarContainer></SidebarContainer>
            <div className="content-wrapper">
                <ContentHeader
                    titulo={"Listado de Factruras"}
                    breadCrumb1={"Inicio"}
                    breadCrumb2={"Facturas"}
                    ruta1={"/home"}
                />

                <section className="content">
                    <div className="card">
                    <div className="card-header">
                            <h3 className="card-title"><Link to={"/facturas-crear"} className="btn btn-block btn-primary btn-sm">Crear Factura</Link></h3>
                            <div className="card-tools">
                                <button type="button" className="btn btn-tool" data-card-widget="collapse" title="Collapse">
                                    <i className="fas fa-minus" />
                                </button>
                                <button type="button" className="btn btn-tool" data-card-widget="remove" title="Remove">
                                    <i className="fas fa-times" />
                                </button>
                            </div>
                        </div>
                        <div className="card-body">
                            <table className="table table-bordered">
                                <thead>
                                    <tr>
                                        <th style={{ width: "5%" }}>Id</th>
                                        <th style={{ width: "10%" }}>Fecha</th>
                                        <th style={{ width: "25%" }}>Nombre Cliente</th>
                                        <th style={{ width: "15%" }}>Documento Cliente</th>
                                        <th style={{ width: "10%" }}>Telefono Cliente</th>
                                        <th style={{ width: "10%" }}>Direccion Cliente</th>
                                        <th style={{ width: "10%" }}>Codigo pproducto</th>
                                        <th style={{ width: "5%" }}>Cantidad</th>
                                        <th style={{ width: "5%" }}>Descuento</th>
                                        <th style={{ width: "5%" }}>Forma de pagos</th>
                                        <th style={{ width: "5%" }}>Valor Unitario</th>
                                        <th style={{ width: "15%" }}>Subtotal</th>
                                        <th style={{ width: "5%" }}>IVA</th>
                                        <th style={{ width: "15%" }}>Valor Total</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {
                                    facturas.map((item) => ( //map() es un método incorporado en los arreglos, para iterar a través de los elementos dentro de una colección de arreglos en JavaScript. Piensa en él, como un bucle para avanzar de un elemento a otro en una lista, manteniendo el orden y la posición de cada elemento
                                        //Una “key” es un atributo especial string que debes incluir al crear listas de elementos. Vamos a discutir por qué esto es importante en la próxima sección.
                                        //Las keys ayudan a React a identificar que ítems han cambiado, son agregados, o son eliminados. Las keys deben ser dadas a los elementos dentro del array para darle a los elementos una identidad estable:
                                        <tr key={item._id}>
                                            <td>{item._id}</td>
                                            <th>{item.fecha}</th>
                                            <td>{item.nombre_cliente}</td>
                                            <td>{item.documento_cliente}</td>
                                            <td>{item.telefono_cliente}</td>
                                            <td>{item.direccion_cliente}</td>
                                            <td>{item.codigo_producto}</td>
                                            <td>{item.cantidad}</td>
                                            <td>{item.descuento}</td>
                                            <td>{item.forma_de_pagos}</td>
                                            <td>{item.valor_unitario}</td>
                                            <td>{item.subtotal}</td>
                                            <td>{item.IVA}</td>
                                            <td>{item.valor_total}</td>
                                            <td>
                                                <Link  to={`/facturas-editar/${item._id}@${item.fecha}@${item.nombre_cliente}@${item.documento_cliente}@${item.telefono_cliente}@${item.direccion_cliente}@${item.codigo_producto}@${item.cantidad}@${item.descuento}@${item.forma_de_pagos}@${item.valor_unitario}@${item.subtotal}@${item.IVA}@${item.valor_total}`} className="btn btn-sm btn-primary">Editar</Link>&nbsp;&nbsp;
                                                <button onClick={(e) => eliminarFactura(e, item._id)} className="btn btn-sm btn-danger">Borrar</button>

                                            </td>
                                        </tr>
                                    ))}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </section>
            </div>
            <Footer></Footer>
        </div>
    );
};

export default FacturasAdmin;