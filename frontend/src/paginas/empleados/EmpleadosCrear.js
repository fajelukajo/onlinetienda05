import React, { useState, useEffect } from 'react';
import ContentHeader from '../../componentes/ContentHeader';
import Footer from '../../componentes/Footer';
import Navbar from '../../componentes/Navbar';
import SidebarContainer from '../../componentes/SidebarContainer';
import { useNavigate } from 'react-router-dom';
import APIInvoke from '../../utils/APIInvoke'
import swal from 'sweetalert';

const EmpleadosCrear = () => {

    const navigate = useNavigate();

    const [empleado, setEmpleado] = useState({
        documento_empleado: '',
        nombre_empleado: '',
        clase_empleado: '',
        telefono_empleado: '',
        sucursal: '',
    });

    const { documento_empleado, nombre_empleado, clase_empleado, telefono_empleado, sucursal} = empleado;

    useEffect(() => {
        document.getElementById("documento_empleado").focus();
    }, [])

    const onChange = (e) => {
        setEmpleado({
            ...empleado,
            [e.target.name]: e.target.value
        })
    }

    const crearEmpleado = async () => {
        const data = {
            documento_empleado: empleado.documento_empleado,
            nombre_empleado: empleado.nombre_empleado,
            clase_empleado: empleado.clase_empleado,
            telefono_empleado: empleado.telefono_empleado,
            sucursal: empleado.sucursal,
        }

        console.log(JSON.stringify(data))
        const response = await APIInvoke.invokePOST(`/api/empleado`, data);
        const idEmpleado = response._id;

        if (idEmpleado === '') {
            const msg = "El empleado NO fue creado correctamente.";
            swal({
                title: 'Error',
                text: msg,
                icon: 'error',
                buttons: {
                    confirm: {
                        text: 'Ok',
                        value: true,
                        visible: true,
                        className: 'btn btn-danger',
                        closeModal: true
                    }
                }
            });
        } else {
            navigate("/empleados-admin");
            const msg = "El empleado fue creado correctamente.";
            swal({
                title: 'Información',
                text: msg,
                icon: 'success',
                buttons: {
                    confirm: {
                        text: 'Ok',
                        value: true,
                        visible: true,
                        className: 'btn btn-primary',
                        closeModal: true
                    }
                }
            });

            setEmpleado({
                documento_empleado: '',
                nombre_empleado: '',
                clase_empleado: '',
                telefono_empleado: '',
                sucursal: ''
             })
        }
    }

    const onSubmit = (e) => {
        e.preventDefault();
        crearEmpleado();
    }

    return (
        <div className="wrapper">
            <Navbar></Navbar>
            <SidebarContainer></SidebarContainer>
            <div className="content-wrapper">

                <ContentHeader
                    titulo={"Creación de Empleados"}
                    breadCrumb1={"Listado de Empleados"}
                    breadCrumb2={"Creación"}
                    ruta1={"/empleados-admin"}
                />

                <section className="content">
                    <div className="card">
                        <div className="card-header">
                            <div className="card-tools">
                                <button type="button" className="btn btn-tool" data-card-widget="collapse" title="Collapse">
                                    <i className="fas fa-minus" />
                                </button>
                                <button type="button" className="btn btn-tool" data-card-widget="remove" title="Remove">
                                    <i className="fas fa-times" />
                                </button>
                            </div>
                        </div>
                        <div className="card-body">

                        <form onSubmit={onSubmit}>
                            <div className="card-body">
                                    <div className="form-group">
                                        <label htmlFor="nombre">Documento Empleado</label>
                                        <input type="text"
                                            className="form-control"
                                            id="documento_empleado"
                                            name="documento_empleado"
                                            placeholder="Ingrese la documento empleado"
                                            value={documento_empleado}
                                            onChange={onChange}
                                            required
                                        />
                                    </div>
                                </div>

                                <div className="card-body">
                                    <div className="form-group">
                                        <label htmlFor="nombre">Nombre Empleado</label>
                                        <input type="text"
                                            className="form-control"
                                            id="nombre_empleado"
                                            name="nombre_empleado"
                                            placeholder="Ingrese el nombre del empleado"
                                            value={nombre_empleado}
                                            onChange={onChange}
                                            required
                                        />
                                    </div>
                                </div>

                                <div className="card-body">
                                    <div className="form-group">
                                        <label htmlFor="nombre">Clase Empleado</label>
                                        <input type="text"
                                            className="form-control"
                                            id="clase_empleado"
                                            name="clase_empleado"
                                            placeholder="Ingrese la clase de empleado administrativo u operativo"
                                            value={clase_empleado}
                                            onChange={onChange}
                                            required
                                        />
                                    </div>
                                </div>

                                <div className="card-body">
                                    <div className="form-group">
                                        <label htmlFor="nombre">Telefono Empleado</label>
                                        <input type="text"
                                            className="form-control"
                                            id="telefono_empleado"
                                            name="telefono_empleado"
                                            placeholder="Ingrese el telefono del empleado"
                                            value={telefono_empleado}
                                            onChange={onChange}
                                            required
                                        />
                                    </div>
                                </div>

                                <div className="card-body">
                                    <div className="form-group">
                                        <label htmlFor="nombre">Sucursal</label>
                                        <input type="text"
                                            className="form-control"
                                            id="sucursal"
                                            name="sucursal"
                                            placeholder="Ingrese la sucursal del empleado"
                                            value={sucursal}
                                            onChange={onChange}
                                            required
                                        />
                                    </div>
                                </div>

                                <div className="card-footer">
                                    <button type="submit" className="btn btn-primary">Crear</button>
                                </div>
                            </form>

                        </div>
                    </div>
                </section>
            </div>
            <Footer></Footer>
        </div>
    );
}

export default EmpleadosCrear;