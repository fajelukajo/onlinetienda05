import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import ContentHeader from "../../componentes/ContentHeader";
import Footer from "../../componentes/Footer";
import Navbar from "../../componentes/Navbar";
import SidebarContainer from "../../componentes/SidebarContainer";
import APIInvoke from "../../utils/APIInvoke";
import swal from "sweetalert";

const EmpleadosAdmin = () => {
    
    const [empleados, setEmpleados] = useState([]);

    const cargarEmpleados = async () => {
        const response = await APIInvoke.invokeGET("/api/empleado");
        //console.log(response);
        setEmpleados(response);
    };

    useEffect(() => {
        cargarEmpleados();
    }, []);

    //Eliminar proyectos
    const eliminarEmpleado = async (e, idEmpleado) => {
        e.preventDefault();
        const response = await APIInvoke.invokeDELETE(`/api/empleado/${idEmpleado}`);

        if (response.msg === 'empleado eliminado con exito') {
            const msg = "El empleado fue borrado correctamente.";
            swal({
                title: 'Información',
                text: msg,
                icon: 'success',
                buttons: {
                    confirm: {
                        text: 'Ok',
                        value: true,
                        visible: true,
                        className: 'btn btn-primary',
                        closeModal: true
                    }
                }
            });
            cargarEmpleados();
        } else {
            const msg = "el empleado no fue borrado correctamente.";
            swal({
                title: 'Error',
                text: msg,
                icon: 'error',
                buttons: {
                    confirm: {
                        text: 'Ok',
                        value: true,
                        visible: true,
                        className: 'btn btn-danger',
                        closeModal: true
                    }
                }
            });
        }

    }

    return (
        <div className="wrapper">
            <Navbar></Navbar>
            <SidebarContainer></SidebarContainer>
            <div className="content-wrapper">
                <ContentHeader
                    titulo={"Listado de Empleados"}
                    breadCrumb1={"Inicio"}
                    breadCrumb2={"Empleados"}
                    ruta1={"/home"}
                />

                <section className="content">
                    <div className="card">
                    <div className="card-header">
                            <h3 className="card-title"><Link to={"/empleado-crear"} className="btn btn-block btn-primary btn-sm">Crear Empleado</Link></h3>
                            <div className="card-tools">
                                <button type="button" className="btn btn-tool" data-card-widget="collapse" title="Collapse">
                                    <i className="fas fa-minus" />
                                </button>
                                <button type="button" className="btn btn-tool" data-card-widget="remove" title="Remove">
                                    <i className="fas fa-times" />
                                </button>
                            </div>
                        </div>
                        <div className="card-body">
                            <table className="table table-bordered">
                                <thead>
                                    <tr>
                                        <th style={{ width: "5%" }}>Id</th>
                                        <th style={{ width: "15%" }}>Documento Empleado</th>
                                        <th style={{ width: "25%" }}>Nombre Empleado</th>
                                        <th style={{ width: "10%" }}>Clase Empleado</th>
                                        <th style={{ width: "10%" }}>Telefono Empleado</th>
                                        <th style={{ width: "10%" }}>Sucursal</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {
                                    empleados.map((item) => ( //map() es un método incorporado en los arreglos, para iterar a través de los elementos dentro de una colección de arreglos en JavaScript. Piensa en él, como un bucle para avanzar de un elemento a otro en una lista, manteniendo el orden y la posición de cada elemento
                                        //Una “key” es un atributo especial string que debes incluir al crear listas de elementos. Vamos a discutir por qué esto es importante en la próxima sección.
                                        //Las keys ayudan a React a identificar que ítems han cambiado, son agregados, o son eliminados. Las keys deben ser dadas a los elementos dentro del array para darle a los elementos una identidad estable:
                                        <tr key={item._id}>
                                            <td>{item._id}</td>
                                            <th>{item.documento_empleado}</th>
                                            <td>{item.nombre_empleado}</td>
                                            <td>{item.clase_empleado}</td>
                                            <td>{item.telefono_empleado}</td>
                                            <td>{item.sucursal}</td>
                                            <td>
                                                <Link  to={`/empleados-editar/${item._id}@${item.documento_empleado}@${item.nombre_empleado}@${item.clase_empleado}@${item.telefono_empleado}@${item.sucursal}`} className="btn btn-sm btn-primary">Editar</Link>&nbsp;&nbsp;
                                                <button onClick={(e) => eliminarEmpleado(e, item._id)} className="btn btn-sm btn-danger">Borrar</button>

                                            </td>
                                        </tr>
                                    ))}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </section>
            </div>
            <Footer></Footer>
        </div>
    );
};

export default EmpleadosAdmin;