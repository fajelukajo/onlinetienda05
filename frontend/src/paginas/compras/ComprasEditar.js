import React, { useState, useEffect } from 'react';
import ContentHeader from '../../componentes/ContentHeader';
import Footer from '../../componentes/Footer';
import Navbar from '../../componentes/Navbar';
import SidebarContainer from '../../componentes/SidebarContainer';
import { useNavigate, useParams } from 'react-router-dom';
import APIInvoke from '../../utils/APIInvoke'
import swal from 'sweetalert';

const ComprasEditar = () => {

    const navigate = useNavigate();

    const { idcompra } = useParams();
    let arreglo = idcompra.split('@');
    
    const productoCompra = arreglo[1];
    const proveedorCompra = arreglo[2];
    const cantidadCompra = arreglo[3];
    const formaPagoCompra = arreglo[4];
    const valorPagarCompra = arreglo[5];
    const descripcionCompra = arreglo[6];




    console.log(arreglo);

    const [compra, setProyecto] = useState({
        producto: productoCompra,
        proveedor: proveedorCompra,
        cantidad:cantidadCompra,
        formaPago: formaPagoCompra,
        valorPagar: valorPagarCompra,
        descripcion: descripcionCompra

    });

    const { producto, proveedor, cantidad, formaPago, valorPagar, descripcion  } = compra;

    useEffect(() => {
        document.getElementById("producto").focus();
    }, [])

    const onChange = (e) => {
        setProyecto({
            ...compra,
            [e.target.name]: e.target.value
        })
    }

    const editarCompra = async () => {
        let arreglo = idcompra.split('@');
        const idCompra = arreglo[0];
        

        const data = {
            producto: compra.producto,
            proveedor: compra.proveedor,
            cantidad: compra.cantidad,
            formaPago: compra.formaPago,
            valorPagar: compra.valorPagar,
            descripcion: compra.descripcion

        }

        const response = await APIInvoke.invokePUT(`/api/compras/${idCompra}`, data);
        const idCompraEditada = response._id

        if (idCompraEditada !== idCompra) {
            const msg = "La compra no se editó correctamente.";
            swal({
                title: 'Error',
                text: msg,
                icon: 'error',
                buttons: {
                    confirm: {
                        text: 'Ok',
                        value: true,
                        visible: true,
                        className: 'btn btn-danger',
                        closeModal: true
                    }
                }
            });

        } else {
            navigate("/compras-admin");
            const msg = "La compra fue editada correctamente.";
            swal({
                title: 'Información',
                text: msg,
                icon: 'success',
                buttons: {
                    confirm: {
                        text: 'Ok',
                        value: true,
                        visible: true,
                        className: 'btn btn-primary',
                        closeModal: true
                    }
                }
            });
        }
    }

    const onSubmit = (e) => {
        e.preventDefault();
        editarCompra();
    }

    return (
        <div className="wrapper">
            <Navbar></Navbar>
            <SidebarContainer></SidebarContainer>
            <div className="content-wrapper">

                <ContentHeader
                    titulo={"Edición de Compras"}
                    breadCrumb1={"Listado de Compras"}
                    breadCrumb2={"Edición"}
                    ruta1={"/compras-admin"}
                />

                <section className="content">
                    <div className="card">
                        <div className="card-header">
                            <div className="card-tools">
                                <button type="button" className="btn btn-tool" data-card-widget="collapse" title="Collapse">
                                    <i className="fas fa-minus" />
                                </button>
                                <button type="button" className="btn btn-tool" data-card-widget="remove" title="Remove">
                                    <i className="fas fa-times" />
                                </button>
                            </div>
                        </div>
                        <div className="card-body">

                        <form onSubmit={onSubmit}>
                                <div className="card-body">
                                    <div className="form-group">
                                        <label htmlFor="nombre">Producto</label>
                                        <input type="text"
                                            className="form-control"
                                            id="producto"
                                            name="producto"
                                            placeholder="Ingrese el nombre del producto"
                                            value={producto}
                                            onChange={onChange}
                                            required
                                        />
                                    </div>
                                </div>
                                <div className="card-body">
                                    <div className="form-group">
                                        <label htmlFor="nombre">Proveedor</label>
                                        <input type="text"
                                            className="form-control"
                                            id="proveedor"
                                            name="proveedor"
                                            placeholder="Ingrese el proveedor"
                                            value={proveedor}
                                            onChange={onChange}
                                            required
                                        />
                                    </div>
                                </div>

                                <div className="card-body">
                                    <div className="form-group">
                                        <label htmlFor="nombre">cantidad</label>
                                        <input type="number"
                                            className="form-control"
                                            id="cantidad"
                                            name="cantidad"
                                            placeholder="Ingrese la cantidad"
                                            value={cantidad}
                                            onChange={onChange}
                                            required
                                        />
                                    </div>
                                </div>
                                <div className="card-body">
                                    <div className="form-group">
                                        <label htmlFor="nombre">Forma de Pago</label>
                                        <input type="text"
                                            className="form-control"
                                            id="formaPago"
                                            name="formaPago"
                                            placeholder="Ingrese la forma de pago"
                                            value={formaPago}
                                            onChange={onChange}
                                            
                                        />
                                    </div>
                                </div>

                                <div className="card-body">
                                    <div className="form-group">
                                        <label htmlFor="nombre">Valor a Pagar</label>
                                        <input type="number"
                                            className="form-control"
                                            id="valorPagar"
                                            name="valorPagar"
                                            placeholder="Ingrese el valor a pagar"
                                            value={valorPagar}
                                            onChange={onChange}
                                            required
                                        />
                                    </div>
                                </div>
                                <div className="card-body">
                                    <div className="form-group">
                                        <label htmlFor="nombre">Descripción</label>
                                        <input type="text"
                                            className="form-control"
                                            id="descripcion"
                                            name="descripcion"
                                            placeholder="descripción"
                                            value={descripcion}
                                            onChange={onChange}
                                            
                                        />
                                    </div>
                                </div>
                                <div className="card-footer">
                                    <button type="submit" className="btn btn-primary">Editar</button>
                                </div>
                            </form>

                        </div>
                    </div>
                </section>
            </div>
            <Footer></Footer>
        </div>
    );
}

export default ComprasEditar;