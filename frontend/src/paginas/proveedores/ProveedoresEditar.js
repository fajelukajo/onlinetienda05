import React, { useState, useEffect } from 'react';
import ContentHeader from '../../componentes/ContentHeader';
import Footer from '../../componentes/Footer';
import Navbar from '../../componentes/Navbar';
import SidebarContainer from '../../componentes/SidebarContainer';
import { useNavigate, useParams } from 'react-router-dom';
import APIInvoke from '../../utils/APIInvoke'
import swal from 'sweetalert';

const ProveedoresEditar = () => {

    const navigate = useNavigate();

    const { idproveedor } = useParams();
    let arreglo = idproveedor.split('@');
    
    const nombreProveedor = arreglo[1];
    const ciudadProveedor = arreglo[2];
    const emailProveedor = arreglo[3] + '@' + arreglo[4];
    const webProveedor = arreglo[5];
    const tipoNegocioProveedor = arreglo[6];
    const lineaTelefonicaProveedor = arreglo[7];




    console.log(arreglo);

    const [proveedor, setProyecto] = useState({
        nombre: nombreProveedor,
        ciudad: ciudadProveedor,
        email:emailProveedor,
        web: webProveedor,
        tipoNegocio: tipoNegocioProveedor,
        lineaTelefonica: lineaTelefonicaProveedor

    });

    const { nombre, ciudad, email, web, tipoNegocio, lineaTelefonica  } = proveedor;

    useEffect(() => {
        document.getElementById("nombre").focus();
    }, [])

    const onChange = (e) => {
        setProyecto({
            ...proveedor,
            [e.target.name]: e.target.value
        })
    }

    const editarProveedor = async () => {
        let arreglo = idproveedor.split('@');
        const idProveedor = arreglo[0];
        

        const data = {
            nombre: proveedor.nombre,
            ciudad: proveedor.ciudad,
            email: proveedor.email,
            web: proveedor.web,
            tipoNegocio: proveedor.tipoNegocio,
            lineaTelefonica: proveedor.lineaTelefonica

        }

        const response = await APIInvoke.invokePUT(`/api/proveedores/${idProveedor}`, data);
        const idProveedorEditado = response._id

        if (idProveedorEditado !== idProveedor) {
            const msg = "El proveedor no fue editado correctamente.";
            swal({
                title: 'Error',
                text: msg,
                icon: 'error',
                buttons: {
                    confirm: {
                        text: 'Ok',
                        value: true,
                        visible: true,
                        className: 'btn btn-danger',
                        closeModal: true
                    }
                }
            });

        } else {
            navigate("/proveedores-admin");
            const msg = "El proveedor fue editado correctamente.";
            swal({
                title: 'Información',
                text: msg,
                icon: 'success',
                buttons: {
                    confirm: {
                        text: 'Ok',
                        value: true,
                        visible: true,
                        className: 'btn btn-primary',
                        closeModal: true
                    }
                }
            });
        }
    }

    const onSubmit = (e) => {
        e.preventDefault();
        editarProveedor();
    }

    return (
        <div className="wrapper">
            <Navbar></Navbar>
            <SidebarContainer></SidebarContainer>
            <div className="content-wrapper">

                <ContentHeader
                    titulo={"Edición de Proveedores"}
                    breadCrumb1={"Listado de Proveedores"}
                    breadCrumb2={"Edición"}
                    ruta1={"/proveedores-admin"}
                />

                <section className="content">
                    <div className="card">
                        <div className="card-header">
                            <div className="card-tools">
                                <button type="button" className="btn btn-tool" data-card-widget="collapse" title="Collapse">
                                    <i className="fas fa-minus" />
                                </button>
                                <button type="button" className="btn btn-tool" data-card-widget="remove" title="Remove">
                                    <i className="fas fa-times" />
                                </button>
                            </div>
                        </div>
                        <div className="card-body">

                            <form onSubmit={onSubmit}>
                                <div className="card-body">
                                    <div className="form-group">
                                        <label htmlFor="nombre">Nombre</label>
                                        <input type="text"
                                            className="form-control"
                                            id="nombre"
                                            name="nombre"
                                            placeholder="Ingrese el nombre del proveedor"
                                            value={nombre}
                                            onChange={onChange}
                                            required
                                        />
                                    </div>
                                </div>
                                <div className="card-body">
                                    <div className="form-group">
                                        <label htmlFor="nombre">Ciudad</label>
                                        <input type="text"
                                            className="form-control"
                                            id="ciudad"
                                            name="ciudad"
                                            placeholder="Ingrese la ciudad"
                                            value={ciudad}
                                            onChange={onChange}
                                            required
                                        />
                                    </div>
                                </div>

                                <div className="card-body">
                                    <div className="form-group">
                                        <label htmlFor="nombre">Email</label>
                                        <input type="email"
                                            className="form-control"
                                            id="email"
                                            name="email"
                                            placeholder="Ingrese email del proveedor"
                                            value={email}
                                            onChange={onChange}
                                            
                                        />
                                    </div>
                                </div>
                                <div className="card-body">
                                    <div className="form-group">
                                        <label htmlFor="nombre">Web</label>
                                        <input type="text"
                                            className="form-control"
                                            id="web"
                                            name="web"
                                            placeholder="Ingrese web del proveedor"
                                            value={web}
                                            onChange={onChange}
                                            
                                        />
                                    </div>
                                </div>

                                <div className="card-body">
                                    <div className="form-group">
                                        <label htmlFor="nombre">Tipo de Negocio</label>
                                        <input type="text"
                                            className="form-control"
                                            id="tipoNegocio"
                                            name="tipoNegocio"
                                            placeholder="Tipo de negocio"
                                            value={tipoNegocio}
                                            onChange={onChange}
                                            required
                                        />
                                    </div>
                                </div>
                                <div className="card-body">
                                    <div className="form-group">
                                        <label htmlFor="nombre">Telefono</label>
                                        <input type="text"
                                            className="form-control"
                                            id="lineaTelefonica"
                                            name="lineaTelefonica"
                                            placeholder="Línea Telefónica"
                                            value={lineaTelefonica}
                                            onChange={onChange}
                                            required
                                        />
                                    </div>
                                </div>
                                <div className="card-footer">
                                    <button type="submit" className="btn btn-primary">Editar</button>
                                </div>
                            </form>

                        </div>
                    </div>
                </section>
            </div>
            <Footer></Footer>
        </div>
    );
}

export default ProveedoresEditar;