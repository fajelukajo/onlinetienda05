import React, { useState, useEffect } from 'react';
import ContentHeader from '../../componentes/ContentHeader';
import Footer from '../../componentes/Footer';
import Navbar from '../../componentes/Navbar';
import SidebarContainer from '../../componentes/SidebarContainer';
import { useNavigate } from 'react-router-dom';
import APIInvoke from '../../utils/APIInvoke'
import swal from 'sweetalert';

const ProveedoresCrear = () => {

    const navigate = useNavigate();

    const [proveedor, setProveedor] = useState({
        nombre: '',
        ciudad: '',
        email:'',
        web: '',
        tipoNegocio: '',
        lineaTelefonica: ''
    });

    const { nombre, ciudad, email, web, tipoNegocio, lineaTelefonica  } = proveedor;

    useEffect(() => {
        document.getElementById("nombre").focus();
    }, [])

    const onChange = (e) => {
        setProveedor({
            ...proveedor,
            [e.target.name]: e.target.value
        })
    }

    const crearProveedor = async () => {
        const data = {
            nombre: proveedor.nombre,
            ciudad: proveedor.ciudad,
            email: proveedor.email,
            web: proveedor.web,
            tipoNegocio: proveedor.tipoNegocio,
            lineaTelefonica: proveedor.lineaTelefonica

        }

        const response = await APIInvoke.invokePOST(`/api/proveedores`, data);
        const idProveedor = response._id;

        if (idProveedor === '') {
            const msg = "El proveedor NO fue creado correctamente.";
            swal({
                title: 'Error',
                text: msg,
                icon: 'error',
                buttons: {
                    confirm: {
                        text: 'Ok',
                        value: true,
                        visible: true,
                        className: 'btn btn-danger',
                        closeModal: true
                    }
                }
            });
        } else {
            navigate("/proveedores-admin");
            const msg = "El proveedor fue creado correctamente.";
            swal({
                title: 'Información',
                text: msg,
                icon: 'success',
                buttons: {
                    confirm: {
                        text: 'Ok',
                        value: true,
                        visible: true,
                        className: 'btn btn-primary',
                        closeModal: true
                    }
                }
            });

            setProveedor({
                nombre: '',
                ciudad: '',
                email:'',
                web: '',
                tipoNegocio: '',
                lineaTelefonica:''
             })
        }
    }

    const onSubmit = (e) => {
        e.preventDefault();
        crearProveedor();
    }

    return (
        <div className="wrapper">
            <Navbar></Navbar>
            <SidebarContainer></SidebarContainer>
            <div className="content-wrapper">

                <ContentHeader
                    titulo={"Creación de Proveedores"}
                    breadCrumb1={"Listado de Proveedores"}
                    breadCrumb2={"Creación"}
                    ruta1={"/proveedores-admin"}
                />

                <section className="content">
                    <div className="card">
                        <div className="card-header">
                            <div className="card-tools">
                                <button type="button" className="btn btn-tool" data-card-widget="collapse" title="Collapse">
                                    <i className="fas fa-minus" />
                                </button>
                                <button type="button" className="btn btn-tool" data-card-widget="remove" title="Remove">
                                    <i className="fas fa-times" />
                                </button>
                            </div>
                        </div>
                        <div className="card-body">

                            <form onSubmit={onSubmit}>
                                <div className="card-body">
                                    <div className="form-group">
                                        <label htmlFor="nombre">Nombre</label>
                                        <input type="text"
                                            className="form-control"
                                            id="nombre"
                                            name="nombre"
                                            placeholder="Ingrese el nombre del proveedor"
                                            value={nombre}
                                            onChange={onChange}
                                            required
                                        />
                                    </div>
                                </div>
                                <div className="card-body">
                                    <div className="form-group">
                                        <label htmlFor="nombre">Ciudad</label>
                                        <input type="text"
                                            className="form-control"
                                            id="ciudad"
                                            name="ciudad"
                                            placeholder="Ingrese la ciudad"
                                            value={ciudad}
                                            onChange={onChange}
                                            required
                                        />
                                    </div>
                                </div>

                                <div className="card-body">
                                    <div className="form-group">
                                        <label htmlFor="nombre">email</label>
                                        <input type="email"
                                            className="form-control"
                                            id="email"
                                            name="email"
                                            placeholder="Ingrese el email"
                                            value={email}
                                            onChange={onChange}
                                            
                                        />
                                    </div>
                                </div>
                                <div className="card-body">
                                    <div className="form-group">
                                        <label htmlFor="nombre">web</label>
                                        <input type="text"
                                            className="form-control"
                                            id="web"
                                            name="web"
                                            placeholder="Ingrese la página web del proveedor"
                                            value={web}
                                            onChange={onChange}
                                            
                                        />
                                    </div>
                                </div>

                                <div className="card-body">
                                    <div className="form-group">
                                        <label htmlFor="nombre">Negocio</label>
                                        <input type="text"
                                            className="form-control"
                                            id="tipoNegocio"
                                            name="tipoNegocio"
                                            placeholder="Tipo de Negocio"
                                            value={tipoNegocio}
                                            onChange={onChange}
                                            required
                                        />
                                    </div>
                                </div>
                                <div className="card-body">
                                    <div className="form-group">
                                        <label htmlFor="nombre">Telefono</label>
                                        <input type="text"
                                            className="form-control"
                                            id="lineaTelefonica"
                                            name="lineaTelefonica"
                                            placeholder="Línea Telefónica"
                                            value={lineaTelefonica}
                                            onChange={onChange}
                                            required
                                        />
                                    </div>
                                </div>
                                <div className="card-footer">
                                    <button type="submit" className="btn btn-primary">Crear</button>
                                </div>
                            </form>

                        </div>
                    </div>
                </section>
            </div>
            <Footer></Footer>
        </div>
    );
}

export default ProveedoresCrear;