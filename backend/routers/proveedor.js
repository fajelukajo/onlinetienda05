
const express = require('express');
const router = express.Router();
const proveedorController = require('../controllers/proveedorController');

// rutas CRUD

router.get('/', proveedorController.mostrarProveedores);
router.post('/', proveedorController.crearProveedor);
router.get('/:id', proveedorController.obtenerProveedor);
router.put('/:id', proveedorController.actualizarProveedor);
router.delete('/:id', proveedorController.eliminarProveedor);

module.exports = router;

