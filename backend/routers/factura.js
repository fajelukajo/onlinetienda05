const express = require('express');
const router = express.Router();
const facturaController = require('../controllers/facturaControllers');

// rutas CRUD

router.get('/', facturaController.mostrarFactura);
router.post('/', facturaController.crearFactura);
router.get('/:id', facturaController.obtenerFactura);
router.put('/:id', facturaController.actualizarFactura);
router.delete('/:id', facturaController.eliminarFactura);

module.exports = router;