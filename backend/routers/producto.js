// Armamos las Rutas para nuestro producto
const express = require('express');
const router = express.Router();
const productoController = require('../controllers/productoController');
const auth = require("../middleware/auth");
const { check } = require("express-validator");
// rutas CRUD

router.get('/', productoController.mostrarProductos);
router.post('/', productoController.crearProducto);
router.get('/:id', productoController.obtenerProducto);
router.put('/:id', productoController.actualizarProducto);
router.delete('/:id', productoController.eliminarProducto);

module.exports = router;