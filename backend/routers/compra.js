
const express = require('express');
const router = express.Router();
const compraController = require('../controllers/compraController');

// rutas CRUD

router.get('/', compraController.mostrarCompras);
router.post('/', compraController.crearCompra);
router.get('/:id', compraController.obtenerCompra);
router.put('/:id', compraController.actualizarCompra);
router.delete('/:id', compraController.eliminarCompra);

module.exports = router;

