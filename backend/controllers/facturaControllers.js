const Factura = require('../models/factura');

exports.crearFactura = async (req,res) => {

    try{
        let factura;
         // creamos nuestro empleado
         factura = new Factura(req.body);
         await factura.save();
         res.send(factura);

}catch (error) {
    console.log(error);
    res.status(500).send("hay un error al recibir los datos");
}
}

exports.mostrarFactura =async (req,res) =>{

try{
   const factura = await Factura.find();
  res.json(factura)

} catch (error){
    console.log(error)
    res.status(500).send("hay un error al recibir los datos");
}
}

exports.obtenerFactura = async (req,res) =>{
 try{
let factura = await Factura.findById(req.params.id);
if (!factura){
    res.status(404).json({msg: 'la factura no existe'})
}
res.json(factura);

 }catch (error){
    console.log(error)
    res.status(500).send("hay un error al recibir los datos");

}
}

exports.eliminarFactura = async (req,res) =>{
    try{
        let factura = await Factura.findById(req.params.id);
        if (!factura){
            res.status(404).json({msg: 'la factura no existe'})
        }
        await Factura.findByIdAndRemove({ _id: req.params.id})
        res.json({msg: 'factura eliminada con exito'});
}catch (error){
    console.log(error)
    res.status(500).send("hay un error al recibir los datos");

}
}

exports.actualizarFactura = async (req, res) => {
    try {
        const { 
            fecha,
            nombre_cliente,
            documento_cliente,
            telefono_cliente,
            direccion_cliente,
            codigo_producto,
            cantidad,
            descuento,
            forma_de_pagos,
            valor_unitario,
            subtotal,
            IVA,
            valor_total,
        } = req.body;
        let factura = await Factura.findById(req.params.id);
        if (!factura) {
            res.status(404).json({ msg: 'la factura no existe' })
        }
        factura.fecha = fecha;
        factura.nombre_cliente = nombre_cliente;
        factura.documento_cliente = documento_cliente;
        factura.telefono_cliente = telefono_cliente;
        factura.direccion_cliente = direccion_cliente;
        factura.codigo_producto = codigo_producto;
        factura.cantidad = cantidad;
        factura.descuento = descuento;
        factura.forma_de_pagos = forma_de_pagos;
        factura.valor_unitario = valor_unitario;
        factura.subtotal = subtotal;
        factura.IVA = IVA;
        factura.valor_total = valor_total;

        factura = await Factura.findOneAndUpdate({ _id: req.params.id }, factura, { new: true })
        res.json(factura);

    } catch (error) {
        console.log(error)
        res.status(500).send("hay un error al recibir los datos");
    }
}