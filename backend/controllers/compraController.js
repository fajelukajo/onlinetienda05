const Compra = require("../models/Compra");

exports.crearCompra = async (req, res) => {

    try {
        let compra;        
        compra = new Compra(req.body);
        await compra.save();
        res.send(compra);

    } catch (error) {
        console.log(error);
        res.status(500).send("hay un error al recibir los datos");
    }
}

exports.mostrarCompras = async (req, res) => {

    try {
        const compras = await Compra.find();
        res.json(compras);

    } catch (error) {
        console.log(error)
        res.status(500).send("hay un error al recibir los datos");
    }
}

exports.obtenerCompra = async (req, res) => {
    try {
        let compra = await Compra.findById(req.params.id);
        if (!compra) {
            res.status(404).json({ msg: 'la compra no existe' })
        }
        res.json(compra);

    } catch (error) {
        console.log(error)
        res.status(500).send("hay un error al recibir los datos");

    }
}

exports.eliminarCompra    = async (req, res) => {
    try {
        let compra = await Compra.findById(req.params.id);
        if (!compra) {
            res.status(404).json({ msg: 'la compra no existe' })
        }
        await Compra.findByIdAndRemove({ _id: req.params.id })
        res.json({ msg: 'compra eliminado con exito' });
    } catch (error) {
        console.log(error)
        res.status(500).send("hay un error al recibir los datos");

    }
}

exports.actualizarCompra = async (req, res) => {
    try {
        const { producto, proveedor, cantidad, formaPago, valorPagar, descripcion } = req.body;
        let compra = await Compra.findById(req.params.id);
        if (!compra) {
            res.status(404).json({ msg: 'la compra no existe' })
        }
        compra.producto = producto;
        compra.proveedor = proveedor;
        compra.cantidad = cantidad;
        compra.formaPago = formaPago;
        compra.valorPagar = valorPagar;
        compra.descripcion = descripcion;

        compra = await Compra.findOneAndUpdate({ _id: req.params.id }, compra, { new: true })
        res.json(compra);

    } catch (error) {
        console.log(error)
        res.status(500).send("hay un error al recibir los datos");
    }
}
