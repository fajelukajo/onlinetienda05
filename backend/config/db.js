const mongoose = require('mongoose');

const dotenv = require('dotenv').config();

const conectarBD = async () => {

    try {
        await mongoose.connect(process.env.MONGO_URL, {

        });
        console.log(" la base de datos esta conectada ");
    }
    catch (error) {
        console.log(error);
        process.exit(1); // detenemos app 
    }
}

module.exports = conectarBD;