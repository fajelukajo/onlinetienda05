const express = require('express');
const conectarBD = require('./config/db');
const cors = require('cors');



const app = express();
const PORT = process.env.PORT || 5000;

// conectar DB
conectarBD();
app.use(cors());
app.use(express.json());
app.use('/api/auth', require('./routers/auth'));
app.use('/api/productos', require('./routers/producto'));
app.use('/api/categorias', require('./routers/categoria'))
app.use('/api/empleado', require('./routers/empleado'));
app.use('/api/factura', require('./routers/factura'));
app.use('/api/proveedores', require('./routers/proveedor'));
app.use('/api/compras', require('./routers/compra'));
app.use('/api/usuario', require('./routers/usuario'));


//muestra mensaje en el navegador 
//Rutas
app.get("/", (req, res) => {
    res.send("Bienvenidos esta configurado su servidor");
});

//Añadir para mi servidor escuche por un puerto
app.listen(PORT, () => console.log('El servidor esta conectado', PORT));
