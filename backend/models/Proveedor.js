const mongoose = require('mongoose');

const proveedorSchema = mongoose.Schema({
    nombre: {
        type: String,
        required: true
    },
    ciudad: {
        type: String,
        required: true
    },
    email: {
        type: String,
    },
    web: {
        type: String,
    },
    tipoNegocio: {
        type : String,
        required: true
    },
    lineaTelefonica: {
        type : String,
        required: true
    }
});

module.exports = mongoose.model('Proveedor', proveedorSchema);