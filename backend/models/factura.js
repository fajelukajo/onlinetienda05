const mongoose = require('mongoose');

const facturaSchema = mongoose.Schema({
    fecha:{
        type: String,
        required: true
    },
    nombre_cliente:{
        type: String,
        required: true
    },
    documento_cliente:{
        type: Number,
        required: true
    },
    telefono_cliente:{
        type: Number,
        required: true
    },
    direccion_cliente:{
        type: String,
        required: true
    },
    codigo_producto:{
        type: Number,
        required: true
    },
    cantidad:{
        type: String,
        required: true
    },
    descuento:{
        type:Number,
        required: true
    },
    forma_de_pagos: {
        type: String,
        required: true
    },
    valor_unitario: {
        type: Number,
        required: true
    },
    subtotal:{
        type: Number,
        required: true
    },
    IVA:{
        type: Number,
        required: true
    },
    valor_total: {
        type: Number,
        required: true
    }
});

module.exports = mongoose.model('Factura', facturaSchema );