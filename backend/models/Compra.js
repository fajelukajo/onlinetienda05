const mongoose = require('mongoose');

const compraSchema = mongoose.Schema({
    producto: {
        type: String,
        required: true
    },
    proveedor: {
        type: String,
        required: true
    },
    cantidad: {
        type: Number,
        required: true
    },
    formaPago: {
        type: String,
    },
    valorPagar: {
        type : Number,
        required: true
    },
    descripcion: {
        type : String,       
    }
});

module.exports = mongoose.model('Compra', compraSchema);