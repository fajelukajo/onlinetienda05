const mongoose = require('mongoose');

const productoSchema = mongoose.Schema({
    nombre: {
        type: String,
        required: true
    },
    categoria: {
        type: String,
        required: true
    },
    presentacion: {
        type: String,
        required: true
    },
    cantidad: {
        type: Number,
        required: true
    },
    valor_Venta: {
        type: Number,
        required: true
    }
});
module.exports = mongoose.model('producto', productoSchema);