const mongoose = require('mongoose');

const empleadoSchema = mongoose.Schema({
    documento_empleado:{
        type: Number,
        required: true
    },
    nombre_empleado:{
        type: String,
        required: true
    },
    clase_empleado:{
        type: String,
        required: true
    },
    telefono_empleado:{
        type: Number,
        required: true
    },
    sucursal:{
        type: String,
        required: true
    }
});

module.exports = mongoose.model('Empleado', empleadoSchema );